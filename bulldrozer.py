#!/usr/bin/python 
# -*- coding: utf-8 -*-

import argparse
import os
import re
import shutil
import sys
import traceback
import xml.etree.ElementTree as ET
from subprocess import Popen, PIPE
from zipfile import ZipFile


def script_path():
    return os.path.dirname(os.path.realpath(__file__))


PROCYON_PATH = script_path() + '/bin/procyon/procyon-decompiler-0.5.30.jar'
# https://github.com/DexPatcher/dex2jar (forked dex2jar because of
# error: "com.googlecode.d2j.DexException: not support version")
# Should probably look for an alternative soon..
DEX2JAR_PATH = script_path() + '/bin/dex-tools-2.1-20171001-lanchon/d2j-dex2jar.sh'
SMALI_PATH = script_path() + '/bin/smali/smali-2.2.3.jar'
BAKSMALI_PATH = script_path() + '/bin/smali/baksmali-2.2.3.jar'
VDEXEXTRACTOR_PATH = script_path() + '/bin/vdexExtractor/vdexExtractor-0.5.2'
CDEXCONVERTOR_PATH = script_path() + '/bin/cdexConverter/bin/compact_dex_converter'
REPORT_PATH = script_path() + '/report/'
# Two separate Java paths - one for Android Studio and related tools (apkanalyzer), another for
# Procyon (which doesnt work well with Java 9+, so now we bundle JRE 8, until Procyon is updated..)
AS_JAVA_PATH = os.path.expanduser('~') + '/AndroidStudio/jre'  # Surely we are on Linux..?
if not os.path.exists(AS_JAVA_PATH):
    print('WARNING: Please define the path to the Android Studio JRE in "AS_JAVA_PATH" for apkanalyzer to work..')
JAVA8_PATH = script_path() + '/bin/jre/jre1.8.0_281/bin/java'

PERMS_NORMAL = [
    u'android.permission.ACCESS_LOCATION_EXTRA_COMMANDS',
    u'android.permission.ACCESS_NETWORK_STATE',
    u'android.permission.ACCESS_NOTIFICATION_POLICY',
    u'android.permission.ACCESS_WIFI_STATE',
    u'android.permission.BLUETOOTH',
    u'android.permission.BLUETOOTH_ADMIN',
    u'android.permission.BROADCAST_STICKY',
    u'android.permission.CALL_COMPANION_APP',
    u'android.permission.CHANGE_NETWORK_STATE',
    u'android.permission.CHANGE_WIFI_MULTICAST_STATE',
    u'android.permission.CHANGE_WIFI_STATE',
    u'android.permission.DISABLE_KEYGUARD',
    u'android.permission.EXPAND_STATUS_BAR',
    u'android.permission.FOREGROUND_SERVICE',
    u'android.permission.GET_PACKAGE_SIZE',
    u'android.permission.INTERNET',
    u'android.permission.KILL_BACKGROUND_PROCESSES',
    u'android.permission.MANAGE_OWN_CALLS',
    u'android.permission.MODIFY_AUDIO_SETTINGS',
    u'android.permission.NFC',
    u'android.permission.NFC_TRANSACTION_EVENT',
    u'android.permission.READ_SYNC_SETTINGS',
    u'android.permission.READ_SYNC_STATS',
    u'android.permission.RECEIVE_BOOT_COMPLETED',
    u'android.permission.REORDER_TASKS',
    u'android.permission.REQUEST_COMPANION_RUN_IN_BACKGROUND',
    u'android.permission.REQUEST_COMPANION_USE_DATA_IN_BACKGROUND',
    u'android.permission.REQUEST_DELETE_PACKAGES',
    u'android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS',
    u'android.permission.REQUEST_PASSWORD_COMPLEXITY',
    u'android.permission.SET_WALLPAPER',
    u'android.permission.SET_WALLPAPER_HINTS',
    u'android.permission.TRANSMIT_IR',
    u'android.permission.USE_BIOMETRIC',
    u'android.permission.USE_FINGERPRINT',
    u'android.permission.USE_FULL_SCREEN_INTENT',
    u'android.permission.VIBRATE',
    u'android.permission.WAKE_LOCK',
    u'android.permission.WRITE_SYNC_SETTINGS',
    u'com.android.alarm.permission.SET_ALARM',
    u'com.android.launcher.permission.INSTALL_SHORTCUT'
]
PERMS_DANGER = [
    u'android.permission.ACCEPT_HANDOVER',
    u'android.permission.ACCESS_BACKGROUND_LOCATION',
    u'android.permission.ACCESS_COARSE_LOCATION',
    u'android.permission.ACCESS_FINE_LOCATION',
    u'android.permission.ACCESS_MEDIA_LOCATION',
    u'android.permission.ACTIVITY_RECOGNITION',
    u'android.permission.ANSWER_PHONE_CALLS',
    u'android.permission.BODY_SENSORS',
    u'android.permission.CALL_PHONE',
    u'android.permission.CAMERA',
    u'android.permission.GET_ACCOUNTS',
    u'android.permission.PROCESS_OUTGOING_CALLS',
    u'android.permission.READ_CALENDAR',
    u'android.permission.READ_CALL_LOG',
    u'android.permission.READ_CONTACTS',
    u'android.permission.READ_EXTERNAL_STORAGE',
    u'android.permission.READ_PHONE_NUMBERS',
    u'android.permission.READ_PHONE_STATE',
    u'android.permission.READ_SMS',
    u'android.permission.RECEIVE_MMS',
    u'android.permission.RECEIVE_SMS',
    u'android.permission.RECEIVE_WAP_PUSH',
    u'android.permission.RECORD_AUDIO',
    u'android.permission.SEND_SMS',
    u'android.permission.USE_SIP',
    u'android.permission.WRITE_CALENDAR',
    u'android.permission.WRITE_CALL_LOG',
    u'android.permission.WRITE_CONTACTS',
    u'android.permission.WRITE_EXTERNAL_STORAGE',
    u'com.android.voicemail.permission.ADD_VOICEMAIL'
]
PERMS_PROTECTION_LEVEL = {
    u'0x0': 'NORMAL',
    u'0x1': 'DANGEROUS',
}


class DrozerCli:
    def __init__(self):
        self.cmdLog = ''
        self.errors = []
    
    def command(self, command, enableShell=None):
        try:
            p = Popen(command, bufsize=-1, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=enableShell)
            if isinstance(command, tuple):
                command = ' '.join(command)
            self.cmdLog += '%s\n' % (command)
            return p
        except OSError as oe:
            print("[OSError]: Error executing local command: " + command + " Message: {0}".format(oe.strerror))
            traceback.print_exc()
            return None
    
    def writeInput(self, inputStream, data):
        return inputStream.communicate(data + "\n")
       
    def getOutput(self, proc):
        return proc.communicate()
    
    def getOutputLines(self, proc, type='sout'):
        lines = []
        data = self.getOutput(proc)
        if type == 'sout':
            data = data[0]
        if type == 'serr':
            data = data[1]
        if data:
            data = data.splitlines()
            for line in data:
                if line:
                    line = line.rstrip()
                    lines.append(line)
        return lines

    def getExitCode(self, proc):
        if proc is not None:
            proc.wait()
            return proc.returncode
        else:
            return None
        
    def printOutput(self, data):
        if data is not None:
            for line in data:
                print(line)
        
        
class AppPackage:
    def __init__(self, package, framewk):
        self.fwkPath = framewk
        self.name = package
        self.packageName = package
        self.localPath = None
        self.exActNum = 0
        self.exBcastNum = 0
        self.exContNum = 0
        self.exSrvNum = 0
        self.actWeight = 0.2
        self.bcastWeight = 0.9
        self.contWeight = 0.7
        self.srvWeight = 0.9
        self.obPercent = (100.0, 100.0, 100.0)
        self.weighted = True
        self.probes = {}
        self.perms = []
        self.definedPerms = []
        self.weakPerms = {}
        self.weakDefPerms = {}
        self.uid = None
        self.version = None
        self.apkPath = None
        self.dataPath = None
        
    def getExportScore(self):
        if self.weighted:
            return self.getWeightedExports()
        else:
            return self.getNumExports()

    def extract(self, dcli):
        print('Extracting APK package from device..')
        filenameApk = self.apkFile()
        extractPath = '%s/extract' % (self.localPath)
        if not os.path.exists(extractPath):
            os.makedirs(extractPath)
        if os.path.exists('%s/%s' % (extractPath, filenameApk)):
            print('Already extracted!')
            return
        adbPull = dcli.command(('adb', 'pull', self.apkPath, extractPath))
        dcli.getOutput(adbPull)
        # Try looking for .vdex files first (Oreo+)
        optType = 'vdex'
        odex = self.findOptimisedDexFiles(dcli, type='v')
        if not odex:
            # If none found look for .odex files
            optType = 'odex'
            odex = self.findOptimisedDexFiles(dcli)
        if odex:
            print('Found %d .%s file(s) to extract' % (len(odex), optType))
            # Open APK as python zipfile
            apkZip = ZipFile(extractPath + '/' + filenameApk, 'a')
            dexNum = 1
            for filename in apkZip.namelist():
                reMatch = re.match('^classes(\d+)?\.dex$', filename)
                if reMatch and (not reMatch.group(1) or reMatch.group(1) >= dexNum):
                    # classes.dex or classes<num>.dex
                    dexNum += 1
            self._processDexFiles(dcli, apkZip, odex, dexNum, optType)
            if optType == 'vdex':
                # Look for .cdex files first and process them
                pathList = os.listdir(extractPath)
                foundCdex = [path for path in pathList if path.endswith('.cdex')]
                if foundCdex:
                    self._processDexFiles(dcli, apkZip, foundCdex, dexNum, 'cdex', pull=False)
                # Look for .dex files
                pathList = os.listdir(extractPath)
                foundDex = [path for path in pathList if path.endswith('.dex')]
                self._processDexFiles(dcli, apkZip, foundDex, dexNum, 'dex', pull=False)
            # Close the apk zip
            apkZip.close()

    def _processDexFiles(self, dcli, apkZip, dexs, dexNum, optType, pull=True):
        filenameApk = self.apkFile()
        extractPath = '%s/extract' % (self.localPath)
        fwkPath = self.fwkPath
        if not os.path.exists(extractPath):
            os.makedirs(extractPath)
        for dexFile in dexs:
            try:
                dexFileName = dexFile.split('/')[-1]
                dexFileNameNoExt = '.'.join(dexFileName.split('.')[:-1])
                if pull:
                    print('Extracting %s file: %s' % (optType, dexFileName))
                    odexPull = dcli.command(('adb', 'pull', dexFile, extractPath))
                    dcli.getOutput(odexPull)
                if optType == 'dex':
                    print('Processing .dex file: %s' % (dexFileName))
                    pass
                elif optType == 'vdex':
                    print('Using vdexExtractor to convert Oreo+ vdex file to dex..')
                    vdex = dcli.command((VDEXEXTRACTOR_PATH, '-i', extractPath + '/' + dexFileName))
                    dcli.getOutput(vdex)
                    continue
                elif optType == 'odex':
                    smaliPath = extractPath + '/smali/' + dexFileNameNoExt
                    print('Converting to smali with baksmali..')
                    baksmali = dcli.command(('java', '-jar', BAKSMALI_PATH, 'de', '-d', fwkPath, '-o', smaliPath,
                                             extractPath + '/' + dexFileName))
                    dcli.getOutput(baksmali)
                    print('Converting to dex with smali..')
                    smali = dcli.command(('java', '-jar', SMALI_PATH, 'as', smaliPath, '-o',
                                          extractPath + '/' + dexFileNameNoExt + '.dex'))
                    dcli.getOutput(smali)
                elif optType == 'cdex':
                    print('Using compact_dex_converter to convert Pie+ cdex file to dex..')
                    cdex = dcli.command((CDEXCONVERTOR_PATH, extractPath + '/' + dexFileName))
                    dcli.getOutput(cdex)
                    # Stupidly the converter does not allow you to specify output name?
                    # And it produces a file with .new extension.. So try and copy to .dex
                    src = extractPath + '/' + dexFileName + '.new'
                    dst = extractPath + '/' + dexFileNameNoExt + '.dex'
                    if os.path.exists(src):
                        print('Found converted file, copying to %s' % dst)
                        shutil.copy(src, dst)
                    continue
                if dexNum == 1:
                    dexName = 'classes.dex'
                else:
                    dexName = 'classes%d.dex' % (dexNum)
                dexNum += 1
                print('Adding %s to %s as %s' % (dexFileNameNoExt + '.dex', filenameApk, dexName))
                apkZip.write(extractPath + '/' + dexFileNameNoExt + '.dex', arcname=dexName)
            except Exception as e:
                print('Exception extracting and converting %s file %s: %s' % (optType, dexFile, e))
                traceback.print_exc()

    def findOptimisedDexFiles(self, dcli, type='o'):
        apkRoot = os.path.dirname(self.apkPath)
        findOptDex = dcli.command("adb shell 'find " + apkRoot + " -name \"*." + type + "dex\"; echo $?'", enableShell=True)
        optDexList = dcli.getOutputLines(findOptDex)
        if not optDexList:
            return []
        exit = optDexList[-1]
        odexList = optDexList[0:-1]
        # If command had non-zero exit code return empty list
        if int(exit) != 0:
            return []
        # Ignore base.[ov]dex files to avoid dupe classes
        return [odex for odex in odexList if not odex.endswith('/base.' + type + 'dex')]

    def apkFile(self):
        return self.apkPath.split('/')[-1]

    def localApkPath(self):
        return '%s/extract/%s' % (self.localPath, self.apkFile())

    def localD2jPath(self):
        filenameApk = self.apkFile()
        filenameNoExt = filenameApk[:filenameApk.rfind('.')]
        return '%s/extract/%s-dex2jar.jar' % (self.localPath, filenameNoExt)
        
    def getNumExports(self):
        return (self.exActNum + self.exBcastNum + self.exContNum + self.exSrvNum)
    
    def getWeightedExports(self):
        return (self.exActNum * self.actWeight) + \
               (self.exBcastNum * self.bcastWeight) + \
               (self.exContNum * self.contWeight) + \
               (self.exSrvNum * self.srvWeight)

    def getPkgInfo(self, type='P'):
        if type not in ('P', 'C', 'M'):
            return []
        try:
            ret = []
            f = open(self.localPath + '/bd.dex.package.list.txt', 'r')
            pkginfo = f.readlines()
            for line in pkginfo:
                lineSplit = line.split(' ')
                if len(lineSplit) > 1 and lineSplit[0].strip() == type:
                    ret.append(lineSplit[1].strip())
            return ret
        except IOError as ioe:
            print('IOError: %s' % ioe)
            return []

    def getManifestPerms(self):
        mperms = {}
        try:
            tree = ET.parse(self.localPath + '/manifest.xml')
            root = tree.getroot()
            for child in root:
                if child.tag == 'permission':
                    attr = {}
                    for key, val in child.attrib.items():
                        if key.endswith('name'):
                            attr['android:name'] = val
                        elif key.endswith('protectionLevel'):
                            attr['android:protectionLevel'] = val
                    mperms[attr['android:name']] = attr
            return mperms
        except IOError as ioe:
            print('IOError: %s' % ioe)
        except Exception as e:
            print('Exception: %s' % e)
        return {}

    def hasWeakPerms(self):
        if not self.weakPerms:
            return False
        else:
            wprms = []
            for permlist in self.weakPerms.values():
                wprms.extend(permlist)
            return len(wprms) > 0

    def displayWeakPerms(self):
        if not self.weakPerms:
            return 'Weak permissions: None\n'
        else:
            out = 'Weak permissions:\n'
            for key in self.weakPerms.keys():
                for item in self.weakPerms[key]:
                    if key == 'Content Provider':
                        comp = 'content://' + item[0]
                    else:
                        comp = item[0]
                    aType = '' if not item[2] else ' ' + item[2].lower()
                    perm = item[1]
                    if perm == 'null':
                        out += '%s (%s) has null%s permission\n' % (key, comp, aType)
                    elif perm in PERMS_NORMAL:
                        out += '%s (%s) has normal%s permission (%s)\n' % (key, comp, aType,  perm)
                    elif perm in PERMS_DANGER:
                        out += '%s (%s) has dangerous%s permission (%s)\n' % (key, comp, aType, perm)
                    elif perm in self.weakDefPerms.keys():
                        level = self.weakDefPerms[perm]
                        out += '%s (%s) has %s%s defined permission (%s)\n' % (key, comp, level.lower(), aType, perm)
            return out

    def displayWeakDefinedPerms(self):
        if not self.weakDefPerms:
            return '\nWeak defined permissions: None\n'
        else:
            out = '\nWeak defined permissions:\n'
            for key in self.weakDefPerms.keys():
                out += 'Defined permission (%s) has weak protection level (%s)\n' % (key, self.weakDefPerms[key])
        return out
        
    def __lt__(self, other):
        return self.getExportScore() < other.getExportScore()
    
    def __gt__(self, other):
        return self.getExportScore() > other.getExportScore()
    
    def __eq__(self, other):
        return self.getExportScore() == other.getExportScore()
    
    def __repr__(self):
        return '''%s (%s)\nUID: %s\nVersion: %s\nApp data path: %s\nAPK path: %s\nExported resources:\n%d Activities\n%d Broadcast Receivers\n%d Content Providers\n%d Services\nObfuscation percentages:\nPackage: %.2f\nClass: %.2f\nMethod: %.2f%s%s\n''' \
               % (self.name, self.packageName, self.uid, self.version, self.dataPath, self.apkPath, self.exActNum,
                  self.exBcastNum, self.exContNum, self.exSrvNum, self.obPercent[0], self.obPercent[1], self.obPercent[2],
                  self.displayWeakDefinedPerms(), self.displayWeakPerms())
        
        
class BullDrozer:
    def __init__(self, args):
        self.filter = args.filter
        self.contextFilter = []
        self.exclude = args.exclude
        self.excludeReported = args.exreported
        self.includeSystem = args.system
        self.includeGoogle = args.google
        self.weakOnly = args.weak
        self.decompile = args.decompile if not args.decompile_apk else True
        self.manifestApk = args.apk_manifest
        self.decompileApk = args.decompile_apk
        self.getFramework = args.framework
        self.forceSummary = args.force
        self.pkgListPath = args.pkglist
        self.printCommands = args.commands
        self.selectedDevice = None
        self.systemFilter = ['com.android']
        self.googleFilter = ['com.google.android']
        self.reportPath = None
        self.frameworkPath = None
        self.dcli = DrozerCli()
        self.auditPkgs = []
        if not args.auditnum:
            self.maxAuditNum = None
        else:
            self.maxAuditNum = int(args.auditnum)
        
    def runDrozerCommand(self, cmd, opts=[]):
        optstr = ' '.join(opts) if opts else ''
        return self.runCommand('drozer console %s --command "run %s" connect' % (optstr, cmd))

    def runCommand(self, cmd):
        cliCmd = self.dcli.command(cmd, True)
        output = self.dcli.getOutputLines(cliCmd)
        exitCode = self.dcli.getExitCode(cliCmd)
        if exitCode != 0:
            self.dcli.errors.append((exitCode, cmd))
        if output and len(output) > 0:
            return (output[1:], exitCode)
        else:
            return ([], exitCode)
        
    def getAttachedDevices(self):
        devices = {}
        adbDevs = self.dcli.getOutputLines(self.dcli.command(('adb', 'devices', '-l')))
        for line in adbDevs:
            lineSplit = line.split(' ', 1)
            if len(lineSplit) > 1:
                if lineSplit[0] not in ('List', '*'):
                    devices[lineSplit[0]] = lineSplit[1].strip()
        return devices
    
    def writeFile(self, path, data):
        fp = open(path, 'w')
        fp.write(data)
        fp.close()

    def filterByContext(self, pkgList):
        pkgs = []
        for pkg in pkgList:
            contextFilters = []
            for contFilt in self.contextFilter:
                if len(contFilt) == 3:
                    # Component filters (activity, broadcast, service, content provider)
                    if contFilt[0] == 'act':
                        contextFilters.append(self.__compareByOp(pkg.exActNum, contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'bcast':
                        contextFilters.append(self.__compareByOp(pkg.exBcastNum, contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'srv':
                        contextFilters.append(self.__compareByOp(pkg.exSrvNum, contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'cont':
                        contextFilters.append(self.__compareByOp(pkg.exContNum, contFilt[1], contFilt[2]))
                    # Obfuscation filters (package, class, method)
                    elif contFilt[0] == 'pobf':
                        contextFilters.append(self.__compareByOp(pkg.obPercent[0], contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'cobf':
                        contextFilters.append(self.__compareByOp(pkg.obPercent[1], contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'mobf':
                        contextFilters.append(self.__compareByOp(pkg.obPercent[2], contFilt[1], contFilt[2]))
                    # Permission filters (perm list, weak perm list, weak defined perm list)
                    elif contFilt[0] == 'perm':
                        contextFilters.append(self.__compareByOp(pkg.perms, contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'wperm':
                        contextFilters.append(self.__compareByOp(pkg.weakPerms.keys(), contFilt[1], contFilt[2]))
                    elif contFilt[0] == 'wdperm':
                        contextFilters.append(self.__compareByOp(pkg.weakDefPerms.keys(), contFilt[1], contFilt[2]))
                    # UID filter
                    elif contFilt[0] == 'uid':
                        contextFilters.append(self.__compareByOp(pkg.uid, contFilt[1], contFilt[2]))
                    # Apk path prefix filter
                    elif contFilt[0] == 'app':
                        contextFilters.append(self.__compareByOp(pkg.apkPath, contFilt[1], contFilt[2]))
                    # Namespace/package filter
                    elif contFilt[0] == 'pkg':
                        contextFilters.append(self.__compareByOp(pkg.getPkgInfo(), contFilt[1], contFilt[2]))
            if False not in contextFilters:
                pkgs.append(pkg)
        return pkgs

    def __compareByOp(self, pkgVal, op, val):
        if pkgVal is None:
            return False
        if not isinstance(pkgVal, list):
            # Cast the val appropriately based on pkgVal
            typeobj = type(pkgVal)
            val = typeobj(val)
        # Perform the filter op
        if op == '==':
            if isinstance(pkgVal, list):
                return val in pkgVal
            return pkgVal == val
        elif op == '!=':
            if isinstance(pkgVal, list):
                return val not in pkgVal
            return pkgVal != val
        elif op == '>':
            if isinstance(pkgVal, list):
                return len(pkgVal) > val
            return pkgVal > val
        elif op == '<':
            if isinstance(pkgVal, list):
                return len(pkgVal) < val
            return pkgVal < val
        elif op == '>=':
            if isinstance(pkgVal, list):
                return len(pkgVal) >= val
            return pkgVal >= val
        elif op == '<=':
            if isinstance(pkgVal, list):
                return len(pkgVal) <= val
            return pkgVal <= val
        elif op == 'sw':
            return pkgVal.startswith(val)
        elif op == '!sw':
            return not pkgVal.startswith(val)
        elif op == 'ew':
            return pkgVal.endswith(val)
        elif op == '!ew':
            return not pkgVal.endswith(val)
        else:
            return False

    def getContextFilter(self, fname):
        for cfilter in self.contextFilter:
            if cfilter[0] == fname and len(cfilter) == 3:
                return cfilter
        return None

    def __getWeakDefinedPerms(self, appPkg):
        if not appPkg.definedPerms:
            return {}
        mperms = appPkg.getManifestPerms()
        if not mperms:
            return {}
        weakDefPerms = {}
        for dperm in appPkg.definedPerms:
            if dperm in mperms.keys():
                if 'android:protectionLevel' in mperms[dperm] and mperms[dperm]['android:protectionLevel'] in PERMS_PROTECTION_LEVEL.keys():
                    weakDefPerms[dperm] = PERMS_PROTECTION_LEVEL[mperms[dperm]['android:protectionLevel']]
                elif 'android:protectionLevel' not in mperms[dperm]:
                    # Protection level not defined, so defaults to NORMAL
                    weakDefPerms[dperm] = 'NORMAL'
        return weakDefPerms

    def getWeakPerms(self, appPkg):
        # Get weak defined perms first so we can also check them
        weakDefinedPerms = self.__getWeakDefinedPerms(appPkg)
        weakPerms = {}
        types = [
            ('Activity', 'app.activity.info.txt'),
            ('Content Provider', 'app.provider.info.txt'),
            ('Broadcast Receiver', 'app.broadcast.info.txt'),
            ('Service', 'app.service.info.txt'),
        ]
        appPath = appPkg.localPath
        for resType in types:
            repPath = '%s/%s' % (appPath, resType[1])
            if os.path.exists(repPath):
                f = open(repPath, 'r')
                lines = f.readlines()
                items = []
                clsPath = None
                path = ''
                for line in lines:
                    line = line.replace('Authority:', '').strip()
                    # Skip intent filter lines
                    if line.startswith('-'):
                        continue
                    clsRe = re.search('^([\\w. _$-]+)$', line)
                    if clsRe:
                        clsPath = clsRe.group(1)
                        path = ''
                    if clsPath and self.__lineStartsWith(line, ['Permission:', 'Read Permission:', 'Write Permission:']):
                        lineSplit = line.split(' ')
                        aType = None
                        if len(lineSplit) > 2:
                            aType = lineSplit[0]
                        perm = lineSplit[-1]
                        if perm == 'null' or perm in PERMS_NORMAL + PERMS_DANGER + weakDefinedPerms.keys():
                            items.append((clsPath + path, perm, aType))
                    elif clsPath and self.__lineStartsWith(line, ['Path:']):
                        # Only occurs in providers
                        path = self.__getValFromLine(line)
                if items and resType[0] not in weakPerms:
                    weakPerms[resType[0]] = items
        return weakPerms, weakDefinedPerms
    
    def getAppPackageList(self):
        pkgs = []
        # Use adb as its much faster
        pkgList = self.runCommand("adb shell 'pm list packages -f'")
        for pkg in pkgList[0]:
            # Split the line on the equals char, pkg name is last element
            pkg = pkg.split('=')[-1]
            if pkg:
                skip = False
                if self.filter:
                    # If filtering enabled, skip packages that dont match
                    filterMatch = [(f in pkg) for f in self.filter]
                    if not any(filterMatch):
                        skip = True
                for exclude in self.exclude:
                    if exclude in pkg:
                        skip = True
                if not self.includeSystem:
                    for sysFilter in self.systemFilter:
                        if pkg.startswith(sysFilter):
                            skip = True
                if not self.includeGoogle:
                    for googFilter in self.googleFilter:
                        if pkg.startswith(googFilter):
                            skip = True
                if skip:
                    continue

                pkgs.append(AppPackage(pkg, self.frameworkPath))

        if self.pkgListPath:
            try:
                userPkgList = open(self.pkgListPath, 'r')
                # Return a list of packages based on the user defined list (they must exist in the list from adb)
                pnames = [p.name for p in pkgs]
                pkgs = [AppPackage(up.strip(), self.frameworkPath) for up in userPkgList.readlines() if up.strip() in pnames]
            except Exception as e:
                print('Exception loading user defined pkg list: %s' % e)
        return pkgs
    
    def __getValFromLine(self, line):
        lineSplit = line.split(' ')
        if lineSplit:
            return lineSplit[-1].strip()
        else:
            return None

    def __lineStartsWith(self, line, starts):
        line = line.strip()
        for start in starts:
            if line.startswith(start):
                return True
        return False

    def readCachedPermData(self, appPkg):
        if not appPkg.localPath:
            return
        perms = []
        definedPerms = []
        infPath = '%s/app.package.info.txt' % appPkg.localPath
        try:
            f = open(infPath, 'r')
            info = f.readlines()
            f.close()
        except IOError as ioe:
            return []
        permList = False
        permDefineList = False
        for line in info:
            if line.strip().startswith('Uses Permissions:'):
                permList = True
            if line.strip().startswith('Defines Permissions:'):
                permDefineList = True
                permList = False
            if permList and line.strip().startswith('-'):
                perms.append(self.__getValFromLine(line))
            elif permDefineList and line.strip().startswith('-'):
                definedPerms.append(self.__getValFromLine(line))
        return perms, definedPerms

    def readSummaryData(self, appPkg):
        if not appPkg.localPath:
            return
        obP = 100.0
        obC = 100.0
        obM = 100.0
        act = 0
        bcast = 0
        prov = 0
        serv = 0
        sumPath = '%s/summary.txt' % appPkg.localPath
        try:
            f = open(sumPath, 'r')
            summary = f.readlines()
            f.close()
        except IOError as ioe:
            return (obP, obC, obM), (act, bcast, prov, serv)
        for line in summary:
            lineSplit = line.split(' ', 1)
            if len(lineSplit) > 1:
                if lineSplit[0] == 'Package:':
                    obP = float(lineSplit[1])
                elif lineSplit[0] == 'Class:':
                    obC = float(lineSplit[1])
                elif lineSplit[0] == 'Method:':
                    obM = float(lineSplit[1])
                elif lineSplit[1].strip() == 'Activities':
                    act = int(lineSplit[0])
                elif lineSplit[1].strip() == 'Broadcast Receivers':
                    bcast = int(lineSplit[0])
                elif lineSplit[1].strip() == 'Content Providers':
                    prov = int(lineSplit[0])
                elif lineSplit[1].strip() == 'Services':
                    serv = int(lineSplit[0])
        return (obP, obC, obM), (act, bcast, prov, serv)
    
    def getAppInfo(self, appPkgs):
        appNum = len(appPkgs)
        for i, appPkg in enumerate(appPkgs):
            pct = ((float(i) + 1.0) / float(appNum)) * 100.0
            print('[%d/%d %%%.2f] Auditing package: %s' % (i+1, appNum, pct, appPkg.packageName))
            appInfo = self.runDrozerCommand('app.package.info -a %s' % (appPkg.packageName))
            permList = False
            for line in appInfo[0]:
                if line.strip().startswith('Application Label:'):
                    appPkg.name = self.__getValFromLine(line)
                if line.strip().startswith('Version:'):
                    appPkg.version = self.__getValFromLine(line)
                if line.strip().startswith('Data Directory:'):
                    appPkg.dataPath = self.__getValFromLine(line)
                if line.strip().startswith('APK Path:'):
                    appPkg.apkPath = self.__getValFromLine(line)
                if line.strip().startswith('UID:'):
                    appPkg.uid = self.__getValFromLine(line)
                if line.strip().startswith('Uses Permissions:'):
                    permList = True
                if line.strip().startswith('Defines Permissions:'):
                    break
                if permList and line.strip().startswith('-'):
                    appPkg.perms.append(self.__getValFromLine(line))
            if None in (appPkg.dataPath, appPkg.apkPath, appPkg.uid):
                print('Failed to read required package data for package: %s (lost connection?)' % (appPkg.name))
                continue
            # Init report path
            if appPkg.version:
                appPkg.localPath = '%s/%s/%s' % (self.reportPath, appPkg.packageName, appPkg.version)
            else:
                appPkg.localPath = '%s/%s' % (self.reportPath, appPkg.packageName)
            if os.path.exists('%s/summary.txt' % (appPkg.localPath)) and not self.forceSummary:
                print('Audit data exists for %s version %s' % (appPkg.name, appPkg.version))
                appPkg.obPercent, surface = self.readSummaryData(appPkg)
                appPkg.exActNum, appPkg.exBcastNum, appPkg.exContNum, appPkg.exSrvNum = surface[0], surface[1], surface[2], surface[3]
                appPkg.perms, appPkg.definedPerms = self.readCachedPermData(appPkg)
                appPkg.weakPerms, appPkg.weakDefPerms = self.getWeakPerms(appPkg)
                continue
            print('Getting attack surface..')
            self.getAppSurface(appPkg)
            if not os.path.exists(appPkg.localPath):
                os.makedirs(appPkg.localPath)
            appPkg.probes['app.package.info'] = appInfo[0]
            nativeComp = self.runDrozerCommand('scanner.misc.native -a %s' % (appPkg.packageName))
            appPkg.probes['scanner.misc.native'] = nativeComp[0]
            # Need busybox installed for the below
            #readFiles = self.runDrozerCommand('scanner.misc.readablefiles %s' % (appPkg.dataPath))
            #appPkg.probes['scanner.misc.readablefiles'] = readFiles[0]
            #writeFiles = self.runDrozerCommand('scanner.misc.writablefiles %s' % (appPkg.dataPath))
            #appPkg.probes['scanner.misc.writablefiles'] = writeFiles[0]
            #suidFiles = self.runDrozerCommand('scanner.misc.sflagbinaries -t %s' % (appPkg.dataPath))
            #appPkg.probes['scanner.misc.sflagbinaries'] = suidFiles[0]
            if appPkg.exActNum:
                output = self.runDrozerCommand('app.activity.info -v -i -a %s' % (appPkg.packageName))
                appPkg.probes['app.activity.info'] = output[0]
                output = self.runDrozerCommand('scanner.activity.browsable -a %s' % (appPkg.packageName))
                appPkg.probes['scanner.activity.browsable'] = output[0]
            if appPkg.exBcastNum:
                output = self.runDrozerCommand('app.broadcast.info -v -i -a %s' % (appPkg.packageName))
                appPkg.probes['app.broadcast.info'] = output[0]
            if appPkg.exContNum:
                output = self.runDrozerCommand('app.provider.info -v -a %s' % (appPkg.packageName))
                appPkg.probes['app.provider.info'] = output[0]
                output = self.runDrozerCommand('app.provider.finduri %s' % (appPkg.packageName))
                appPkg.probes['app.provider.finduri'] = output[0]
                output = self.runDrozerCommand('scanner.provider.finduris -a %s' % (appPkg.packageName))
                appPkg.probes['scanner.provider.finduris'] = output[0]
                output = self.runDrozerCommand('scanner.provider.injection -a %s' % (appPkg.packageName))
                appPkg.probes['scanner.provider.injection'] = output[0]
                output = self.runDrozerCommand('scanner.provider.sqltables -a %s' % (appPkg.packageName))
                appPkg.probes['scanner.provider.sqltables'] = output[0]
                output = self.runDrozerCommand('scanner.provider.traversal -a %s' % (appPkg.packageName))
                appPkg.probes['scanner.provider.traversal'] = output[0]
            if appPkg.exSrvNum:
                output = self.runDrozerCommand('app.service.info -v -i -a %s' % (appPkg.packageName))
                appPkg.probes['app.service.info'] = output[0]
            # Always extract package for audit
            appPkg.extract(self.dcli)
            # Get package list with apkanalyzer
            plistProc = self.dcli.command("JAVA_HOME=%s apkanalyzer dex packages --defined-only '%s' | grep -E \"[PCM]\s\" | grep -v '<TOTAL>' | awk {'printf (\"%%s %%s\\n\", $1, $NF)'}" % (AS_JAVA_PATH, appPkg.localApkPath()), enableShell=True)
            plistOut = self.dcli.getOutputLines(plistProc)
            appPkg.probes['bd.dex.package.list'] = plistOut
            # Attempt to get a ratio of obfuscated packages, methods and classes
            appPkg.obPercent = self.getObfuscationRatios(plistOut)
            apkManifestOut = self.getPkgManifest(appPkg.packageName, appPkg.localApkPath())
            if apkManifestOut and apkManifestOut[1] == 0 and apkManifestOut[0]:
                self.writeFile('%s/manifest.xml' % (appPkg.localPath), '\n'.join(apkManifestOut[0]))
            else:
                print('Failed to extract manifest.xml file from APK: %s' % appPkg.localApkPath())
            for probe, output in appPkg.probes.items():
                self.writeFile('%s/%s.txt' % (appPkg.localPath, probe), '\n'.join(output))
            # Get used/defined perms
            appPkg.perms, appPkg.definedPerms = self.readCachedPermData(appPkg)
            # Get weak permissions
            appPkg.weakPerms, appPkg.weakDefPerms = self.getWeakPerms(appPkg)
            # Write audit data now
            print('Writing app audit data for: %s (%s)' % (appPkg.packageName, appPkg.localPath))
            self.writeFile('%s/summary.txt' % (appPkg.localPath), str(appPkg))

    def getObfuscationRatios(self, dexList):
        types = ['P', 'C', 'M']
        data = {}
        if not dexList:
            # Not all apps have dex lists it seems
            return 100.0, 100.0, 100.0
        for line in dexList:
            # Split the line to determine type
            lineSplit = line.split(' ')
            for type in types:
                if type not in data.keys():
                    data[type] = {'total': [], 'obf': 0.0}
                if len(lineSplit) > 1:
                    if lineSplit[0] and lineSplit[0] == type:
                        data[type]['total'].append(lineSplit[1])
        for type in types:
            data[type]['obf'] = self.getObfuscationRatio(data[type]['total'], type)
        return data['P']['obf'], data['C']['obf'], data['M']['obf']

    def getFrameworkFiles(self):
        if not os.path.exists(self.frameworkPath):
            print('Creating device framework file path: %s' % (self.frameworkPath))
        elif self.getFramework:
            print('Refreshing framework files at path: %s' % (self.frameworkPath))
            shutil.rmtree(self.frameworkPath)
        else:
            # Already got the framework files and no need to refresh
            return

        # Find the framework path by searching the device for the boot.oat file
        fwkFind = self.dcli.command('adb shell find / -name boot.oat | grep boot.oat', enableShell=True)
        fwkFindOutput = self.dcli.getOutputLines(fwkFind)

        frameworkRootStr = None
        if fwkFindOutput:
            bootOatPath = fwkFindOutput[0]
            # Split path and look for 'framework' in path name - that is the framework root on Nexus5
            pathSplit = bootOatPath.split('/')
            frameworkRoot = []
            for pathEl in pathSplit:
                if not pathEl.strip():
                    continue
                frameworkRoot.append(pathEl)
                if 'framework' in pathEl:
                    break
            frameworkRootStr = '/' + '/'.join(frameworkRoot)
        else:
            # Fallback, check for /system/framework (i.e. Android 11)
            if self.dcli.getExitCode(self.dcli.command('adb shell ls /system/framework', enableShell=True)) == 0:
                frameworkRootStr = '/system/framework'
            else:
                print('Could not find the boot.oat file on the device, cannot determine framework path')
                return

        os.mkdir(self.frameworkPath)
        print('Framework path appears to be: %s' % (frameworkRootStr))
        fwkFileList = self.dcli.command(['adb', 'shell', 'find', frameworkRootStr, '-type', 'f'])
        fwkFiles = self.dcli.getOutputLines(fwkFileList)
        print('Pulling %d framework files from device..' % (len(fwkFiles)))
        for fwkFile in fwkFiles:
            fwkPull = self.dcli.command(['adb', 'pull', fwkFile, self.frameworkPath])
            self.dcli.getOutput(fwkPull)

    def getObfuscationRatio(self, allValues, type):
        totalVals = len(allValues)
        obVal = []
        for val in allValues:
            obValue = val.split('.')[-1]
            if type == 'P':
                # Look at the final part of the split package and try and match to the regex
                if re.match('^[a-z0-9]{1,2}$', obValue) is not None:
                    obVal.append(val)
            elif type == 'C':
                # Look at the final part of the split class and try and match to the regex
                if re.match('^[a-z0-9]{1,3}(\$[a-z0-9]{1,3})?$', obValue) is not None:
                    obVal.append(val)
            elif type == 'M':
                obValue = val
                # Use the whole of the method and try and match to the regex
                if re.match('^[a-z0-9]{1,3}\(([\w+\.,$])*\)$', obValue) is not None:
                    obVal.append(val)
            if val not in obVal and len(obValue) == 1 and (1 <= len(obValue[0]) < 3):
                obVal.append(val)
        if totalVals == 0 or len(obVal) == 0:
            return 0.0
        return (float(len(obVal)) / totalVals) * 100.0
    
    def getAppSurface(self, appPkg):
        appSurface = self.runDrozerCommand('app.package.attacksurface %s' % (appPkg.packageName))
        for line in appSurface[0]:
            lineSplit = line.strip().split(' ')
            if lineSplit[1] == 'activities':
                appPkg.exActNum = int(lineSplit[0])
            elif lineSplit[1] == 'broadcast':
                appPkg.exBcastNum = int(lineSplit[0])
            elif lineSplit[1] == 'content':
                appPkg.exContNum = int(lineSplit[0])
            elif lineSplit[1] == 'services':
                appPkg.exSrvNum = int(lineSplit[0])

    def getPkgManifest(self, pkg, apkPath):
        # Get manifest with apkanalyzer
        apkManifest = self.runCommand("JAVA_HOME=%s apkanalyzer manifest print '%s'" % (AS_JAVA_PATH, apkPath))
        if apkManifest[1] == 0:
            return apkManifest
        else:
            # Fallback to getting manifest with drozer
            return self.runDrozerCommand('app.package.manifest %s' % pkg, opts=['--no-color'])

    def getMfTagAttr(self, tag, attr):
        ns = '{http://schemas.android.com/apk/res/android}'
        return tag.attrib.get('%s%s' % (ns, attr), None)

    def processManifest(self, manifest):
        print(manifest)
        print('\nParsing manifest.xml..')
        try:
            root = ET.XML(manifest)
            for child in root:
                if child.tag == 'permission':
                    name = self.getMfTagAttr(child, 'name')
                    protection = self.getMfTagAttr(child, 'protectionLevel')
                    if not protection:
                        # No protection defined, defaults to NORMAL
                        print('[PERM] Weak default (NORMAL) permission defined: %s' % name)
                    elif protection in PERMS_PROTECTION_LEVEL:
                        print('[PERM] Weak (NORMAL/DANGEROUS) permission defined: %s' % name)
                elif child.tag == 'application':
                    for appchild in child:
                        if appchild.tag in ('activity', 'provider', 'receiver', 'service'):
                            self.isExportedAndAccessibleComponent(appchild)
        except IOError as ioe:
            print('IOError: %s' % ioe)
        except Exception as e:
            print('Exception: %s' % e)

    def isExportedAndAccessibleComponent(self, component):
        name = self.getMfTagAttr(component, 'name')
        exported = self.getMfTagAttr(component, 'exported')
        perm = self.getMfTagAttr(component, 'permission')
        readPerm = self.getMfTagAttr(component, 'readPermission')
        writePerm = self.getMfTagAttr(component, 'writePermission')
        # Components are implicitly exported if not defined, and including an intent filter
        if (not exported and len(component.findall('intent-filter'))) or (exported and exported == 'true'):
            if not perm or perm == 'null':
                # No/null component permissions
                print('[COMP] %s component (%s) is exported and accessible (no permissions defined)' % (component.tag.capitalize(), name))
                return True
            elif readPerm or writePerm:
                # For providers, read/write perms take precedence
                readLevel = self.isDangerousPerm(readPerm)
                writeLevel = self.isDangerousPerm(writePerm)
                if readLevel:
                    print('[COMP] %s component (%s) is exported and accessible (dangerous %s read permission defined: %s)' % (component.tag.capitalize(), readLevel, name, perm))
                if writeLevel:
                    print('[COMP] %s component (%s) is exported and accessible (dangerous %s write permission defined: %s)' % (component.tag.capitalize(), writeLevel, name, perm))
                if readLevel or writeLevel:
                    return True
            elif self.isDangerousPerm(perm):
                # Dangerous acquirable permissions
                print('[COMP] %s component (%s) is exported and accessible (dangerous %s permission defined: %s)' % (component.tag.capitalize(), self.isDangerousPerm(perm), name, perm))
                return True
            #elif perm:
            #    # Maybe acquirable perms?
            #    print('[COMP] %s component (%s) is exported with custom permission: %s' % (component.tag.capitalize(), name, perm))
        return False

    def isDangerousPerm(self, permission):
        if permission and permission in PERMS_NORMAL + PERMS_DANGER:
            return 'DANGER' if permission in PERMS_DANGER else 'NORMAL'
        return None
    
    def run(self):
        if self.manifestApk:
            if os.path.exists(self.manifestApk):
                apkManifestOut = self.runCommand("JAVA_HOME=%s apkanalyzer manifest print '%s'" % (AS_JAVA_PATH, self.manifestApk))
                if apkManifestOut and apkManifestOut[1] == 0 and apkManifestOut[0]:
                    self.processManifest('\n'.join(apkManifestOut[0]))
                else:
                    print('Cant get manifest of APK at path: %s' % self.manifestApk)
            else:
                print('Cant show manifest of APK at invalid path: %s' % self.manifestApk)
        elif self.decompile and self.decompileApk:
            if os.path.exists(self.decompileApk):
                print('Decompiling APK at path: %s' % self.decompileApk)
                apkFileNoExt = self.decompileApk.split('/')[-1].split('.')[0]
                decompileApkPath = os.path.dirname(self.decompileApk)
                decompileSrcPath = '%s/%s_src' % (decompileApkPath, apkFileNoExt)
                decompileD2JPath = '%s/%s-dex2jar.jar' % (decompileApkPath, apkFileNoExt)
                print('Converting to JAR with dex2jar..')
                dex2Jar = self.dcli.command("cd %s; %s '%s'" % (decompileApkPath, DEX2JAR_PATH, self.decompileApk), True)
                self.dcli.getOutput(dex2Jar)
                print('Converting to Java source with Procyon..')
                jar2Src = self.dcli.command("%s -jar %s -o '%s' '%s'" % (JAVA8_PATH, PROCYON_PATH, decompileSrcPath, decompileD2JPath), True)
                self.dcli.getOutput(jar2Src)
            else:
                print('Cant decompile APK at invalid path: %s' % self.decompileApk)
        else:
            self.init()
            self.audit()
            self.report()

        if self.printCommands:
            print('\nAll commands:')
            print('--------------------------')
            for cmd in self.dcli.cmdLog.split('\n'):
                if not cmd.strip():
                    continue
                print('Command: %s' % cmd)
        if self.dcli.errors:
            print('\nCommand failures:')
            print('--------------------------')
            for error in self.dcli.errors:
                print('Exit: %d, Command: %s' % (error[0], error[1]))
        
    def getSummary(self, appPkgs):
        summary = ''
        for appPkg in appPkgs:
            summary += str(appPkg)
        return summary

    def getReported(self, path):
        rep = []
        for content in os.listdir(path):
            rep.append(content)
        return rep
        
    def report(self):
        if self.filter or self.exclude or self.contextFilter:
            print('\n-----------------------\n Filtered packages:\n-----------------------\n')
        elif self.maxAuditNum:
            print('\n-----------------------\n Top %d packages:\n-----------------------\n' % (self.maxAuditNum))
        else:
            print('\n-----------------------\n All packages:\n-----------------------\n')
        self.auditPkgs.sort(reverse=True)
        summary = self.getSummary(self.auditPkgs)
        print(summary)
        if self.decompile:
            for appPkg in self.auditPkgs:
                decompilePath = '%s/extract/src' % (appPkg.localPath)
                if not os.path.exists(decompilePath):
                    os.makedirs(decompilePath)
                else:
                    print('Already decompiled!')
                    continue
                print('Converting to JAR with dex2jar..')
                dex2Jar = self.dcli.command("cd '%s/../'; '%s' '%s'" % (decompilePath, DEX2JAR_PATH, appPkg.localApkPath()), True)
                self.dcli.getOutput(dex2Jar)
                print('Converting to Java source with Procyon..')
                jar2Src = self.dcli.command("%s -jar %s -o '%s' '%s'" % (JAVA8_PATH, PROCYON_PATH, decompilePath, appPkg.localD2jPath()), True)
                self.dcli.getOutput(jar2Src)
        
    def audit(self):
        print('\nStarting audit')
        print('Getting app package list..')
        if self.includeSystem:
            print(' - Including system packages')
        if self.includeGoogle:
            print(' - Including Google packages')
        if self.weakOnly:
            print(' - Only show packages with weak permissions')
        if self.filter:
            filters = self.filter.split(',')
            self.filter = []
            for filter in filters:
                if ':' in filter:
                    filterSplit = filter.split(':')
                    if len(filterSplit) != 3:
                        print('Invalid context filter: %s (format = <filter>:<op>:<value>)' % (filter))
                        sys.exit(7)
                    self.contextFilter.append(filter.split(':'))
                else:
                    self.filter.append(filter)
            if self.filter:
                print(' - Using filter: %s' % (self.filter))
            if self.contextFilter:
                print(' - Using context filter: %s' % (self.contextFilter))
        else:
            self.filter = []
        if self.exclude:
            self.exclude = self.exclude.split(',')
            print(' - Using exclude: %s' % (self.exclude))
        else:
            self.exclude = []
        if self.excludeReported:
            reported = self.getReported(self.reportPath)
            print(' - Excluding already reported packages (%s)' % (reported))
            self.exclude.extend(reported)
        pkgs = self.getAppPackageList()
        if not pkgs:
            print('No packages found')
            sys.exit(6)
        print('Found %d packages' % (len(pkgs)))
        if self.maxAuditNum:
            print('Audit maximum %d packages' % (self.maxAuditNum))
            self.auditPkgs = pkgs[:self.maxAuditNum]
        else:
            self.auditPkgs = pkgs
        print('Getting info on audit packages..\n')
        self.getAppInfo(self.auditPkgs)
        if self.weakOnly:
            print('Only showing packages with weak permissions..')
            wPkgs = [pkg for pkg in self.auditPkgs if pkg.hasWeakPerms()]
            self.auditPkgs = wPkgs
        if self.contextFilter:
            print('Filtering packages by context..')
            self.auditPkgs = self.filterByContext(self.auditPkgs)
            if not self.auditPkgs:
                print('No packages found after filtering')
                sys.exit(7)
        
    def init(self):
        # Check for drozer in PATH
        print('Checking for Drozer in $PATH..')
        dCheck = self.dcli.command(('which', 'drozer'))
        out = self.dcli.getOutputLines(dCheck)
        if out:
            print('Drozer found at: %s' % (out[0]))
        else:
            print('Couldnt find Drozer in $PATH')
            sys.exit(1)
        # Check for adb in PATH
        print('Checking for adb in $PATH..')
        adbCheck = self.dcli.command(('which', 'adb'))
        out = self.dcli.getOutputLines(adbCheck)
        if out:
            print('adb found at: %s' % (out[0]))
        else:
            print('Couldnt find adb in $PATH')
            sys.exit(2)
        # Check for apkanalyzer in PATH
        print('Checking for apkanalyzer in $PATH..')
        apkanalyzerCheck = self.dcli.command(('which', 'apkanalyzer'))
        out = self.dcli.getOutputLines(apkanalyzerCheck)
        if out:
            print('apkanalyzer found at: %s' % (out[0]))
        else:
            print('Couldnt find apkanalyzer in $PATH (https://developer.android.com/studio/command-line/apkanalyzer)')
            sys.exit(2)
        # Check for connected android devices
        print('Checking attached devices..')
        devs = self.getAttachedDevices()
        if not devs:
            print('No devices attached')
            sys.exit(3)
        elif len(devs.keys()) > 1:
            print('Multiple devices attached, please connect a single device')
            sys.exit(3)
        elif len(devs.keys()) == 1:
            self.selectedDevice = devs.items()[0]
        print('Using device: %s (%s)' % (self.selectedDevice[0], self.selectedDevice[1]))
        self.reportPath = REPORT_PATH + self.selectedDevice[0]
        self.frameworkPath = '%s/%s_framework' % (self.reportPath, self.selectedDevice[0])
        if not os.path.exists(self.reportPath):
            print('Creating report folder for device: %s' % (self.selectedDevice[0]))
            os.mkdir(self.reportPath)
        # Set adb port forwards 
        print('Setting adb port forwards..')
        adbPortForward = self.dcli.command(('adb', 'forward', 'tcp:31415', 'tcp:31415'))
        adbPortForwardExit = self.dcli.getExitCode(adbPortForward)
        if adbPortForwardExit != 0:
            print('Problem setting adb port forwards (non-zero exit code: %s)' % (adbPortForwardExit))
            sys.exit(4)
        # Check Drozer agent connection
        print('Checking connection to Drozer agent..')
        drozerAgentList = self.dcli.command(('drozer', 'console', 'devices'))
        drozerAgentListExit = self.dcli.getExitCode(drozerAgentList)
        if drozerAgentListExit != 0:
            print('Problem connecting to Drozer agent, is the embedded server running?')
            sys.exit(5)
        # Get device-specific framework files/libraries
        self.getFrameworkFiles()
                 
        
def printCliHeader():
    dozerText = u'''            
 ▄▄▄▄    █    ██  ██▓     ██▓    ▓█████▄  ██▀███   ▒█████  ▒███████▒▓█████  ██▀███  
▓█████▄  ██  ▓██▒▓██▒    ▓██▒    ▒██▀ ██▌▓██ ▒ ██▒▒██▒  ██▒▒ ▒ ▒ ▄▀░▓█   ▀ ▓██ ▒ ██▒
▒██▒ ▄██▓██  ▒██░▒██░    ▒██░    ░██   █▌▓██ ░▄█ ▒▒██░  ██▒░ ▒ ▄▀▒░ ▒███   ▓██ ░▄█ ▒
▒██░█▀  ▓▓█  ░██░▒██░    ▒██░    ░▓█▄   ▌▒██▀▀█▄  ▒██   ██░  ▄▀▒   ░▒▓█  ▄ ▒██▀▀█▄  
░▓█  ▀█▓▒▒█████▓ ░██████▒░██████▒░▒████▓ ░██▓ ▒██▒░ ████▓▒░▒███████▒░▒████▒░██▓ ▒██▒
░▒▓███▀▒░▒▓▒ ▒ ▒ ░ ▒░▓  ░░ ▒░▓  ░ ▒▒▓  ▒ ░ ▒▓ ░▒▓░░ ▒░▒░▒░ ░▒▒ ▓░▒░▒░░ ▒░ ░░ ▒▓ ░▒▓░
▒░▒   ░ ░░▒░ ░ ░ ░ ░ ▒  ░░ ░ ▒  ░ ░ ▒  ▒   ░▒ ░ ▒░  ░ ▒ ▒░ ░░▒ ▒ ░ ▒ ░ ░  ░  ░▒ ░ ▒░
 ░    ░  ░░░ ░ ░   ░ ░     ░ ░    ░ ░  ░   ░░   ░ ░ ░ ░ ▒  ░ ░ ░ ░ ░   ░     ░░   ░ 
 ░         ░         ░  ░    ░  ░   ░       ░         ░ ░    ░ ░       ░  ░   ░     
      ░                           ░                        ░                        
'''
    print(dozerText.encode('utf-8'))
    print('Drozer (https://github.com/mwrlabs/drozer) automation script :: chutchut\n')


if __name__ == '__main__':
    printCliHeader()
    parser = argparse.ArgumentParser()
    parser.add_argument('--filter', help="Filter packages to audit by package name and/or attack surface")
    parser.add_argument('--exclude', help="Exclude packages to audit by package name")
    parser.add_argument('--exreported', help="Exclude already reported packages in %s" % (REPORT_PATH),
                        action="store_true")
    parser.add_argument('--auditnum', help="Maximum number of packages to audit")
    parser.add_argument('--system', help="Include system packages", action="store_true")
    parser.add_argument('--google', help="Include Google packages", action="store_true")
    parser.add_argument('--weak', help="Only show packages with weak permissions", action="store_true")
    parser.add_argument('--decompile', help="Decompile packages to Java JAR and source code", action="store_true")
    parser.add_argument('--decompile_apk', help="Decompile APK at path", type=str)
    parser.add_argument('--apk_manifest', help="Show manifest of APK at path", type=str)
    parser.add_argument('--framework', help="Force refresh of device framework files", action="store_true")
    parser.add_argument('--force', help="Force refresh of summary data", action="store_true")
    parser.add_argument('--pkglist', help="Path to a file containing package names to audit")
    parser.add_argument('--commands', help="Show cli commands run", action="store_true")
    args = parser.parse_args()
    bdrozer = BullDrozer(args)
    bdrozer.run()
