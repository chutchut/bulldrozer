import pprint

import requests
from bs4 import BeautifulSoup

#
# Get latest system-reserved Android actions
# https://developer.android.com/reference/android/content/Intent
# pip install requests beautifulsoup4 lxml
#

reserved_actions = []


resp = requests.get('https://developer.android.com/reference/android/content/Intent')
if resp:
    soup = BeautifulSoup(resp.text.encode('utf-8'), features='lxml')
    action_section = soup.find('div', id='jd-content')
    if action_section:
        action_section_divs = action_section.find_all('div')
        for div in action_section_divs:
            header = div.find('h3', class_='api-name')
            if header and header.text.startswith('ACTION_'):
                action_note = div.find('p', class_='note')
                if action_note:
                    note_text = action_note.text.replace('\n', '')
                    if 'This is a protected intent that can only be sent by the system' in note_text:
                        reserved_actions.append(header.text)


pp = pprint.PrettyPrinter(indent=1)
print('\nReserved actions:')
pp.pprint(sorted(reserved_actions))
