import os
import traceback
from subprocess import Popen, PIPE
import argparse

#
# Convert odex -> dex with smali/baksmali
#

SMALI_PATH = os.getcwd() + '/smali/smali-2.2.3.jar'
BAKSMALI_PATH = os.getcwd() + '/smali/baksmali-2.2.3.jar'

class DrozerCli:
    def __init__(self):
        self.cmdLog = ''
        self.errors = []

    def command(self, command, enableShell=None):
        try:
            p = Popen(command, bufsize=-1, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=enableShell)
            if isinstance(command, tuple):
                command = ' '.join(command)
            self.cmdLog += '%s\n' % (command)
            return p
        except OSError as oe:
            print("[OSError]: Error executing local command: " + command + " Message: {0}".format(oe.strerror))
            traceback.print_exc()
            return None

    def writeInput(self, inputStream, data):
        return inputStream.communicate(data + "\n")

    def getOutput(self, proc):
        return proc.communicate()

    def getOutputLines(self, proc, type='sout'):
        lines = []
        data = self.getOutput(proc)
        if type is 'sout':
            data = data[0]
        if type is 'serr':
            data = data[1]
        if data:
            data = data.splitlines()
            for line in data:
                if line:
                    line = line.rstrip()
                    lines.append(line)
        return lines

    def getExitCode(self, proc):
        if proc is not None:
            proc.wait()
            return proc.returncode
        else:
            return None

    def printOutput(self, data):
        if data is not None:
            for line in data:
                print(line)


def convert(dcli, fwkPath, src, target):
    file = src.split('/')[-1].split('.')[:-1]
    odexFileNameNoExt = '.'.join(file)
    smaliPath = os.path.dirname(src) + '/smali/' + odexFileNameNoExt
    print('Converting to smali with baksmali..')
    baksmali = dcli.command(('java', '-jar', BAKSMALI_PATH, 'de', '-d', fwkPath, '-o', smaliPath,
                             src))
    dcli.printOutput(dcli.getOutputLines(baksmali))
    print('Converting to dex with smali..')
    smali = dcli.command(('java', '-jar', SMALI_PATH, 'as', smaliPath, '-o',
                          target + '/' + odexFileNameNoExt + '.dex'))
    dcli.printOutput(dcli.getOutputLines(smali))
    print(dcli.cmdLog)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--fwk', help="Framework path")
    parser.add_argument('--src', help="Source odex file")
    parser.add_argument('--dest', help="Destination path")
    args = parser.parse_args()
    if not args.fwk or not args.src or not args.dest:
        print('Missing args')
    elif args.fwk and args.src and args.dest:
        if not os.path.exists(args.src) or not os.path.exists(args.fwk) or not os.path.exists(args.dest):
            print('Invalid paths')
        else:
            dcli = DrozerCli()
            try:
                convert(dcli, args.fwk, args.src, args.dest)
            except Exception as e:
                print('Exception: %s' % e)
