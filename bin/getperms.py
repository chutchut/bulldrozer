import re
import pprint

import requests
from bs4 import BeautifulSoup

#
# Get latest (non-system) Android permissions
# https://developer.android.com/reference/android/Manifest.permission.html
# pip install requests beautifulsoup4 lxml
#

normal = [u'android.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS']  # Inconsistent ui (no 'Protection level:')
dangerous = []
exclude = ['signature', 'privileged']
excludeText = 'Not for use by third-party applications'


def getSiblings(el, tag):
    sibs = []
    for sib in el.next_siblings:
        if sib.name:
            if sib.name == tag:
                sibs.append(sib)
            else:
                break
    return sibs


def isNonSysPerm(pLevels):
    for level in pLevels:
        if level in exclude:
            return False
    return True


resp = requests.get('https://developer.android.com/reference/android/Manifest.permission.html')
if resp:
    soup = BeautifulSoup(resp.text.encode('utf-8'), features='lxml')
    constHdr = soup.find('h2', class_='api-section', string='Constants')
    if constHdr:
        permConstants = getSiblings(constHdr, 'div')
        for constant in permConstants:
            title = re.search('Constant\sValue:\s+"(.+)"', constant.get_text())
            reMatch = re.search('Protection\slevel:\s([\w|]+)', constant.get_text())
            if title and reMatch:
                permTitle = title.group(1)
                pLevel = reMatch.group(1)
                pLevelSplit = pLevel.split('|')
                if isNonSysPerm(pLevelSplit) and excludeText not in constant.get_text():
                    if 'dangerous' in pLevelSplit and permTitle not in dangerous:
                        dangerous.append(permTitle)
                    elif 'normal' in pLevelSplit and permTitle not in normal:
                        normal.append(permTitle)
                elif excludeText in constant.get_text():
                    print('Ignoring excluded permission %s (%s)' % (permTitle, pLevel))
                else:
                    print('Ignoring system permission %s (%s)' % (permTitle, pLevel))

pp = pprint.PrettyPrinter(indent=1)
print('\nNormal permissions:')
pp.pprint(sorted(normal))
print('\nDangerous permissions:')
pp.pprint(sorted(dangerous))
